# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright notice
# and this notice are preserved.
# This file is offered as-is, without any warranty.

SCRIPTS=$(shell find source/*)

DESTDIR=
PREFIX=usr/local
INST_DIR=$(DESTDIR)/$(PREFIX)
INST_BIN_DIR=$(INST_DIR)/bin

.PHONY: all install uninstall

all:
	@echo "All prepared."

install:
	mkdir -p "$(INST_BIN_DIR)/"
	cp source/send-config "$(INST_BIN_DIR)/"

uninstall:
	rm -f "$(INST_BIN_DIR)/send-config"

clean:
	@echo "Nothing to do."
