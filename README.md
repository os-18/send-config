## send-config

A small script for copying the configuration of several programs.

---

## Dependencies

This project uses OpenSSH.

---

## Installation from sources

```bash
make install
```

Scripts will be installed to `/usr/local/bin` (`bin` is a directory for
programs, `PREFIX` is `usr/local`).

The project Makefile contains variables:

* `DESTDIR` specifies the root directory for installing (empty, by default);
* `PREFIX` points to the base directory like `usr/local` or `usr`.

Installation directory is defined as `$(DESTDIR)/$(PREFIX)` in the Makefile.

You can install scripts of this project to any other directory,
like `$HOME/.local` (script files will be in `$HOME/.local/bin/`):

```bash
make install DESTDIR=$HOME PREFIX=.local
```

Uninstall:

```bash
make uninstall
```

If you installed the project in an alternative directory (like $HOME/.local):

```bash
make uninstall DESTDIR=$HOME PREFIX=.local
```

